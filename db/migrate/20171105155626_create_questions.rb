class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.string :start
      t.string :end
      t.string :options, array: true
      t.string :correct

      t.timestamps
    end
  end
end
