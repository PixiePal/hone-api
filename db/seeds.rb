# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Question.delete_all

Question.create! (
  [
    {
      start: "My food was warmer",
      end: "yours.",
      options: ['then', 'as', 'than'],
      correct: "than"
    },
    {
      start: 'Ana saw a monster. The monster frightened',
      end: '.',
      options: ['her', 'she', 'him', 'it'],
      correct: 'her'
    },
    {
      start: 'Nobody',
      end: 'that she is innocent.',
      options: ['believes', 'believed'],
      correct: 'believes'
    },
    {
      start: 'Martin Luther King Jr.',
      end: 'for human rights.',
      options: ['fighted', 'fought', 'fights'],
      correct: 'fought'
    },
    {
      start: 'Cars pollute the environment,',
      end: '?',
      options: ["don't they", 'do they', "isn't it", 'is it', "aren't they"],
      correct: "don't they"
    }
  ]
)
puts "Questions seeded!"
