require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest
  test 'routes version' do
    assert_generates '/questions', { controller: 'questions', action: 'index' }
  end
end
