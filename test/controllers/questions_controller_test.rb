require 'test_helper'

class QuestionsControllerTest < ActionDispatch::IntegrationTest
  test "get all questions" do
    get '/questions'

    assert_equal 200, response.status
    refute_empty response.body
  end
end
