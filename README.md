Ruby on Rails API for the 'Hone' grammar practice tool.

Uses Rails 5 in API mode and connects through JSON to the 'Hone' React Web App.

Created by -Orsolya Toth-Gati.
